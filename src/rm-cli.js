#!/usr/bin/env node

// rm-cli
// by Jeremy Morton
// Implements some functionalities of 'rm' on the commandline in a cross-platform way.

"use strict";

const Promise = require("Bluebird");
const fs = Promise.promisifyAll(require("fs"));
const glob = Promise.promisifyAll(require("glob"));
const parser = require("./cli-parser.js");

// Parse CLI params
let options = parser.parse(process.argv);
if (options.error !== null) {
	console.log(options.error);
	process.exit(1);
}

// Do unlinking of specified paths
let errorOccurred = false;
let allPaths = Array.prototype.slice.call(options.paths);  // Ensure allPaths is Array instance

if (allPaths.length === 0) {
	if (!options.suppressWarnings) {
		console.log("No paths specified!");
	}
	process.exit(0);
}

let globPath = function(path, warnOnEmpty) {
	let foundPaths = new Array();
	return glob.globAsync(
		path,
		{
			// Match Unix-style 'hidden' .dotfiles when wildcard matching?
			dot: options.globDotfiles
		}
	)
	.then(function(files) {
		if (files.length === 0) {
			if (warnOnEmpty) {
				console.log(`Cannot remove '${path}': No such file or directory`);
			}
		}
		else {
			files.forEach(function(filepath) { foundPaths.push(filepath); });
		}

		return foundPaths;
	})
	.catch(function(err) {
		errorOccurred = true;
		if (!options.suppressErrors) {
			console.log(`Cannot glob '${path}': ${err.message}`);
			throw err;
		}
	});
};

let unlinkPaths = function(paths) {
	// Unlink each file (maybe recursively remove directories too)
	if (!paths) { paths = []; }
	return Promise.each(
		paths,
		(path) => {  // Standard arrow function; curly brackets, result has to be returned explicitly
			// Note that we're *already* in a promise callback here, hence our always returning a Promise.  The Promise.each(...) has
			// called back our code.  It's OK to do async work now such as disk I/O like unlinking.
			// Also not that async work is not the same as expensive work.  In this example our async work might be I/O, which is fine
			// and will not lock up our thread.  But expensive CPU work all done in JS will still make our thread unresponsive as JS
			// cannot break out mid-function to execute other code in the same thread, even if the function is running asynchronously.
			// Promises should look like synchronous code and if they start to look too different you're probably using them wrong.
			if (options.verbose) {
				console.log(`Removing: ${path}`);
			}
			return (
				fs.statAsync(path)
				.catch((err) => {
					let errMsg = `Cannot stat '${path}': ${err.message}`;
					if (options.recursive) {
						errorOccurred = true;
						throw new Error(errMsg);
					}
					else if (!options.suppressErrors) {
						console.log(errMsg);
					}
				})
				.then(function(stats) {
					if (typeof stats === "undefined") { return; }

					if (!(stats.isFile() || (stats.isDirectory() && options.recursive))) {
						let errMsg;
						if (stats.isDirectory()) {
							errMsg = `Cannot remove '${path}': Is a directory`;
						}
						else {
							errMsg = `Cannot remove '${path}': Is not a file`;
						}

						if (options.recursive) {
							errorOccurred = true;
							throw new Error(errMsg);
						}
						else if (!options.suppressWarnings) {
							// There are three semantically different ways to get out of a Promise callback:
							// 1: return a non-Promise value.  The next 'then' callback will run immediately, and see the returned value
							//    as the resolution value (this can just be 'undefined' if no return statement was given, too).
							// 2: return a Promise value.  The next 'then' callback will only be run once the Promise value has been
							//    resolved.
							// 3: throw an Error.  The next 'then' callback will not be run and a 'catch' callback will be if it exists.
							//
							// Here, we're hitting case 1; there's no return value, but still no Error thrown, so it counts as a
							// successful (fulfilled) Promise.  Its resolution value will be "undefined" which is OK; we've nothing to return.
							console.log(errMsg);
						}
					}
					else if (stats.isFile()) {
						return fs.unlinkAsync(path)
						.catch((err) => {
							errorOccurred = true;
							let errMsg = `Cannot remove '${path}': ${err.message}`;
							if (options.recursive) {
								throw new Error(errMsg);
							}
							else if (!options.suppressErrors) {
								console.log(errMsg);
							}
						});
					}
					else if (stats.isDirectory()) {
						// Directory; recursively remove its contents then remove it
						return (
							globPath(path + "/*")
							.then(unlinkPaths)
							.then(() => fs.rmdirAsync(path))  // Arrow function expression; no curly brackets, evaluates statement and returns result
						);
					}
				})
			);
		}
	);
};

// Expand paths based on wildcards etc. (globbing support), then try and unlink everything
Promise.each(
	allPaths,
	function(currentPath) {
		return (
			globPath(currentPath, !options.suppressWarnings)
			.then(unlinkPaths)
			.catch(function(err) {
				if (options.recursive) {
					errorOccurred = true;
				}
				if (!options.suppressErrors) {
					errorOccurred = true;
					console.log(`Cannot remove '${currentPath}': ${err.message}`);
				}
			})
		);
	}
)
.then(function() {
	if (errorOccurred) {
		process.exit(1);
	}
	else {
		process.exit(0);
	}
});
