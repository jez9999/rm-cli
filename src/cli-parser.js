"use strict";

module.exports = {
	// Parses commandline arguments
	// commandlineArgs: The commandline arguments; should be process.argv.
	parse: function(commandlineArgs) {
		let result = {
			error: null,
			suppressWarnings: false,
			suppressErrors: false,
			recursive: false,
			verbose: false,
			paths: []
		};

		let parsingVariadic = false;
		let count = 1;
		while (commandlineArgs.length > ++count) {
			let thisArg = commandlineArgs[count];
			if (thisArg[0] === "-") {
				if (parsingVariadic) {
					result.error = "Invalid commandline format '" + thisArg + "'.  Options must come before filenames.";
					return result;
				}

				let thisOption = thisArg.substring(1);
				switch (thisOption) {
					case "s":
						result.suppressWarnings = true;
						break;

					case "ss":
						result.suppressWarnings = true;
						result.suppressErrors = true;
						break;

					case "r":
						result.recursive = true;
						break;

					case "v":
						result.verbose = true;
						break;

					case "d":
						result.globDotfiles = true;
						break;

					default:
						result.error = "Unrecognized option '-" + thisOption + "'.";
						return result;
				}
			}
			else {
				parsingVariadic = true;
				result.paths.push(thisArg);
			}
		}

		return result;
	}
};
