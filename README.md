# rm-cli
Implements some functionalities of 'rm' on the commandline in a cross-platform way.

Examples of use:

    rm-cli somefile.txt
    rm-cli -s somefile1.txt somefile2.jpg somefile3.pdf
    rm-cli -ss dir1/somefile.png
    rm-cli dir1/dir2/*
    rm-cli -v dir3/* somefile4.txt foo/bar/anotherfile.js
	rm-cli -r complexdir1 complexdir2/*

This tool supports "globbing", meaning that wildcards can be specified on the commandline, as indicated above in the usage examples.

Multiple file or wildcard paths can be specified as parameters and rm-cli will attempt to remove all of them.

Supported parameters (must come *before* the file/wildcard paths):

    -s   Suppress warnings
    -ss  Suppress warnings and errors
    -v   Verbose mode
    -d   Glob wildcards match .dotfiles
    -r   Recursively delete matched directories as well
