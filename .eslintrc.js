module.exports = {
	env: {
		node: true,
		es6: true
	},
	extends: "eslint:recommended",
	rules: {
		"indent": ["error", "tab", {"SwitchCase": 1}],
		"semi": ["error", "always", {"omitLastInOneLineBlock": false}],
		"no-console": "off",
		"no-unused-vars": "off",
		"no-empty": ["error", {"allowEmptyCatch": true}],
		"no-mixed-spaces-and-tabs": ["error", "smart-tabs"],
		"no-constant-condition": ["error", {"checkLoops": false}],
		"strict": ["error", "safe"],
		"no-var": "error",
		"no-unreachable": "warn",
		"curly": "error",
		"eqeqeq": "error",
		"no-throw-literal": "error",
		"no-undef-init": "error",
		"no-use-before-define": ["error", "nofunc"],
		"no-path-concat": "error",
		"new-parens": "error",
		"no-trailing-spaces": "error",
		"unicode-bom": ["error", "never"],
		"no-cond-assign": "off"
	}
};
